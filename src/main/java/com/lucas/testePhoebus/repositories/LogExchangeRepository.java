package com.lucas.testePhoebus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lucas.testePhoebus.models.LogExchange;

public interface LogExchangeRepository extends JpaRepository<LogExchange, Long> {
	
	LogExchange findById(long id);

}
