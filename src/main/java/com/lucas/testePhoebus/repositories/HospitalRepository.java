package com.lucas.testePhoebus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.lucas.testePhoebus.models.Hospital;

public interface HospitalRepository extends JpaRepository<Hospital, Long>{
	
	Hospital findById (long id);

}
