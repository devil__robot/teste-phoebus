package com.lucas.testePhoebus.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lucas.testePhoebus.models.Hospital;
import com.lucas.testePhoebus.repositories.HospitalRepository;

@Service
public class ReportService {
	
	@Autowired
	private HospitalRepository hospitalRepository;
	
	@Autowired
	public ReportService (HospitalRepository hospitalRepository) {
		this.hospitalRepository = hospitalRepository;
	}
	
	
	public List<Hospital> listHospitalFull(){
		List<Hospital> allHospital = hospitalRepository.findAll();
		List<Hospital> filtredHospital = new ArrayList<>();
		for (Hospital hospital : allHospital) {
			if (hospital.getPercentageCapacity() >= 90) {
				filtredHospital.add(hospital);
			}
		}
		
		return filtredHospital;
		
	}
	
	
	public List<Hospital> listHospitalEmpity(){
		List<Hospital> allHospital = hospitalRepository.findAll();
		List<Hospital> filtredHospital = new ArrayList<>();
		for (Hospital hospital : allHospital) {
			if (hospital.getPercentageCapacity() < 90) {
				filtredHospital.add(hospital);
			}
		}
		
		return filtredHospital;
		
	}
	
	
	public double percentageHospitalFull() {
		double percentage = 0;
		List<Hospital> allHospital = hospitalRepository.findAll();
		double contAllHospital = allHospital.size();
		double contFiltredHospital = 0;
		if (contAllHospital > 0) {
			for (Hospital hospital : allHospital) {
				if (hospital.getPercentageCapacity() >= 90) {
					contFiltredHospital++;
					
				}
			}
			percentage = (contFiltredHospital/contAllHospital)*100;
			
		}
		
		
		return percentage;
	}
	
	
	public double percentageHospitalEmpity() {
		double percentage = 0;
		List<Hospital> allHospital = hospitalRepository.findAll();
		double contAllHospital = allHospital.size();
		double contFiltredHospital = 0;
		if (contAllHospital > 0) {
			for (Hospital hospital : allHospital) {
				if (hospital.getPercentageCapacity() < 90) {
					contFiltredHospital++;
					
				}
			}
			percentage = (contFiltredHospital/contAllHospital)*100;
			
		}
		
		
		return percentage;
	}
	
	
	public List<Double> averageRecoursePerHospital(){
		List<Double> averageRecourse= new ArrayList<>();
		List<Hospital> allHospital = hospitalRepository.findAll();
		double contAllAmbulance = 0;
		double contAllNurse = 0;
		double contAllPhysician = 0;
		double contAllRespirator = 0;
		double contAllTomograph = 0;
		double contAllHospital = allHospital.size();
		
		for (Hospital hospital : allHospital) {
			contAllAmbulance+=hospital.getAmbulance().getQuantityAmbulance();
			contAllNurse+=hospital.getNurse().getQuantityNurse();
			contAllPhysician+=hospital.getPhysician().getQuantityPhysician();
			contAllRespirator+=hospital.getRespirator().getQuantityRespirator();
			contAllTomograph+=hospital.getTomograph().getQuantityTomograph();
			
		}
		
		averageRecourse.add((contAllAmbulance/contAllHospital));
		averageRecourse.add((contAllNurse/contAllHospital));
		averageRecourse.add((contAllPhysician/contAllHospital));
		averageRecourse.add((contAllRespirator/contAllHospital));
		averageRecourse.add((contAllTomograph/contAllHospital));
		
		
		
		return averageRecourse;
	}
	
	
	public Hospital oldHospitalFull() {
		List<Hospital> allHospital = hospitalRepository.findAll();
		Hospital oldHospital = new Hospital();
		oldHospital = allHospital.get(0);
		
		for (Hospital hospital : allHospital) {
			if(hospital.getSuperLotationDate()!= null) {
				if(oldHospital.getSuperLotationDate()==null) {
					oldHospital = hospital;
				}
				else if(hospital.getSuperLotationDate().compareTo(oldHospital.getSuperLotationDate()) < 0) {
					oldHospital = hospital;
					
				}
			}
		}
		
		return oldHospital;
	}
	
	
	public Hospital oldHospitalUnder() {
		List<Hospital> allHospital = hospitalRepository.findAll();
		Hospital oldHospital = new Hospital();
		oldHospital = allHospital.get(0);
		
		for (Hospital hospital : allHospital) {
			if(hospital.getUnderLotationDate()!= null) {
				if(oldHospital.getUnderLotationDate()==null) {
					oldHospital = hospital;
				}
				else if(hospital.getUnderLotationDate().compareTo(oldHospital.getUnderLotationDate()) < 0) {
					oldHospital = hospital;
					
				}
			}
		}
		
		return oldHospital;
	}

}
