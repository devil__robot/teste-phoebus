package com.lucas.testePhoebus.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lucas.testePhoebus.models.Hospital;
import com.lucas.testePhoebus.repositories.HospitalRepository;
import com.lucas.testePhoebus.repositories.LogExchangeRepository;

@Service
public class HospitalService {
	
	private HospitalRepository hospitalRepository;
	
	
	@Autowired
	private LogExchangeService logExchangeService;
	
	@Autowired
	public HospitalService(HospitalRepository hospitalRepository) {
		this.hospitalRepository = hospitalRepository;
	}
	
	
	public boolean isCapacity90(Hospital hospital) {
		
		boolean result = false;
		
		if (hospital.getPercentageCapacity() > 90) {
			result = true;
		}
		return result;
	}
	
	
	public boolean traderBalancePhysician(Hospital hospitalReceive, Hospital hospitalSend) {
		boolean result = false;
		
		if (hospitalReceive.getPhysician().finalScore() == hospitalSend.getPhysician().finalScore()) {
			result = true;
		}
		
		return result;
	}
	
	
	public boolean realizeOperationExchangePhysician(Hospital hospitalReceive, Hospital hospitalSend, double quantityReceive, double quantitySend) {
		boolean result = false;
		
		
		if (quantitySend > hospitalSend.getPhysician().getQuantityPhysician()) {
			quantitySend = hospitalSend.getPhysician().getQuantityPhysician();
		}
		
		if (quantityReceive > hospitalReceive.getPhysician().getQuantityPhysician()) {
			quantityReceive = hospitalSend.getPhysician().getQuantityPhysician();
		}
		
		
		
		hospitalReceive.getPhysician().setQuantityPhysician(
				hospitalReceive.getPhysician().getQuantityPhysician() + quantitySend - quantityReceive
			);
		
		hospitalSend.getPhysician().setQuantityPhysician(
				hospitalSend.getPhysician().getQuantityPhysician() + quantityReceive - quantitySend
			);
		
		if (traderBalancePhysician(hospitalReceive, hospitalSend) == true) {
			hospitalRepository.save(hospitalReceive);
			hospitalRepository.save(hospitalSend);
			result = true;
			
		}
		
		else if (isCapacity90(hospitalSend) || isCapacity90(hospitalReceive)) {
			hospitalRepository.save(hospitalReceive);
			hospitalRepository.save(hospitalSend);
			result = true;
		}
		
		
		return result;
	}
	
	
	public boolean resourseChangePhysician(Long hospitalIdReceive, Long hospitalIdSend, double quantityReceive, double quantitySend) {
		boolean[] result = {false};
		Optional<Hospital> optionalHospitalI = hospitalRepository.findById(hospitalIdReceive);
		optionalHospitalI.ifPresent(hospitalReceive -> {
			Optional<Hospital> optionalHospitalII = hospitalRepository.findById(hospitalIdSend);
			optionalHospitalII.ifPresent(hospitalSend -> {				
				
				result[0] = this.realizeOperationExchangePhysician(hospitalReceive, hospitalSend, quantityReceive, quantitySend);
				 
				
				
				
			});
			
		});
		
		if (result[0] == true) {
			logExchangeService.saveLogExchangeSerive(hospitalIdReceive,
					hospitalIdSend, "Médicos", quantityReceive, quantitySend);			
			
		}
		
		return result[0];
	}
	
	
	
	
	
	//Nurse
	
	public boolean traderBalanceNurse(Hospital hospitalReceive, Hospital hospitalSend) {
		boolean result = false;
		
		if (hospitalReceive.getNurse().finalScore() == hospitalSend.getNurse().finalScore()) {
			result = true;
		}
		
		return result;
	}
	
	
	public boolean realizeOperationExchangeNurse(Hospital hospitalReceive, Hospital hospitalSend, double quantityReceive, double quantitySend) {
		boolean result = false;
		
		
		if (quantitySend > hospitalSend.getNurse().getQuantityNurse()) {
			quantitySend = hospitalSend.getNurse().getQuantityNurse();
		}
		
		if (quantityReceive > hospitalReceive.getNurse().getQuantityNurse()) {
			quantityReceive = hospitalSend.getNurse().getQuantityNurse();
		}
		
		
		
		hospitalReceive.getNurse().setQuantityNurse(
				hospitalReceive.getNurse().getQuantityNurse() + quantitySend - quantityReceive
			);
		
		hospitalSend.getNurse().setQuantityNurse(
				hospitalSend.getNurse().getQuantityNurse() + quantityReceive - quantitySend
			);
		
		if (traderBalanceNurse(hospitalReceive, hospitalSend) == true) {
			hospitalRepository.save(hospitalReceive);
			hospitalRepository.save(hospitalSend);
			result = true;
			
		}
		
		else if (isCapacity90(hospitalSend) || isCapacity90(hospitalReceive)) {
			hospitalRepository.save(hospitalReceive);
			hospitalRepository.save(hospitalSend);
			result = true;
		}
		
		
		return result;
	}
	
	
	public boolean resourseChangeNurse(Long hospitalIdReceive, Long hospitalIdSend, double quantityReceive, double quantitySend) {
		boolean[] result = {false};
		Optional<Hospital> optionalHospitalI = hospitalRepository.findById(hospitalIdReceive);
		optionalHospitalI.ifPresent(hospitalReceive -> {
			Optional<Hospital> optionalHospitalII = hospitalRepository.findById(hospitalIdSend);
			optionalHospitalII.ifPresent(hospitalSend -> {				
				
				result[0] = this.realizeOperationExchangeNurse(hospitalReceive, hospitalSend, quantityReceive, quantitySend);
				 
				
				
				
			});
			
		});
		
		if (result[0] == true) {
			logExchangeService.saveLogExchangeSerive(hospitalIdReceive,
					hospitalIdSend, "Enfermeiros", quantityReceive, quantitySend);			
			
		}
		
		
		return result[0];
	}
	
	
	
	
	//Respirator
	
	
	
		public boolean traderBalanceRespirator(Hospital hospitalReceive, Hospital hospitalSend) {
			boolean result = false;
			
			if (hospitalReceive.getRespirator().finalScore() == hospitalSend.getRespirator().finalScore()) {
				result = true;
			}
			
			return result;
		}
		
		
		public boolean realizeOperationExchangeRespirator(Hospital hospitalReceive, Hospital hospitalSend, double quantityReceive, double quantitySend) {
			boolean result = false;
			
			
			if (quantitySend > hospitalSend.getRespirator().getQuantityRespirator()) {
				quantitySend = hospitalSend.getRespirator().getQuantityRespirator();
			}
			
			if (quantityReceive > hospitalReceive.getRespirator().getQuantityRespirator()) {
				quantityReceive = hospitalSend.getRespirator().getQuantityRespirator();
			}
			
			
			
			hospitalReceive.getRespirator().setQuantityRespirator(
					hospitalReceive.getRespirator().getQuantityRespirator() + quantitySend - quantityReceive
				);
			
			hospitalSend.getRespirator().setQuantityRespirator(
					hospitalSend.getRespirator().getQuantityRespirator() + quantityReceive - quantitySend
				);
			
			if (traderBalanceRespirator(hospitalReceive, hospitalSend) == true) {
				hospitalRepository.save(hospitalReceive);
				hospitalRepository.save(hospitalSend);
				result = true;
				
			}
			
			else if (isCapacity90(hospitalSend) || isCapacity90(hospitalReceive)) {
				hospitalRepository.save(hospitalReceive);
				hospitalRepository.save(hospitalSend);
				result = true;
			}
			
			
			return result;
		}
		
		
		
		public boolean resourseChangeRespirator(Long hospitalIdReceive, Long hospitalIdSend, double quantityReceive, double quantitySend) {
			boolean[] result = {false};
			Optional<Hospital> optionalHospitalI = hospitalRepository.findById(hospitalIdReceive);
			optionalHospitalI.ifPresent(hospitalReceive -> {
				Optional<Hospital> optionalHospitalII = hospitalRepository.findById(hospitalIdSend);
				optionalHospitalII.ifPresent(hospitalSend -> {				
					
					result[0] = this.realizeOperationExchangeRespirator(hospitalReceive, hospitalSend, quantityReceive, quantitySend);
					 
					
					
					
				});
				
			});
			
			
			if (result[0] == true) {
				logExchangeService.saveLogExchangeSerive(hospitalIdReceive,
						hospitalIdSend, "Respirador", quantityReceive, quantitySend);			
				
			}
			
			
			return result[0];
		}
		
		
		
		//Tomograph
		
		
		
			public boolean traderBalanceTomograph(Hospital hospitalReceive, Hospital hospitalSend) {
				boolean result = false;
				
				if (hospitalReceive.getTomograph().finalScore() == hospitalSend.getTomograph().finalScore()) {
					result = true;
				}
				
				return result;
			}
			
			
			
			public boolean realizeOperationExchangeTomograph(Hospital hospitalReceive, Hospital hospitalSend, double quantityReceive, double quantitySend) {
				boolean result = false;
				
				
				if (quantitySend > hospitalSend.getTomograph().getQuantityTomograph()) {
					quantitySend = hospitalSend.getTomograph().getQuantityTomograph();
				}
				
				if (quantityReceive > hospitalReceive.getTomograph().getQuantityTomograph()) {
					quantityReceive = hospitalSend.getTomograph().getQuantityTomograph();
				}
				
				
				
				hospitalReceive.getTomograph().setQuantityTomograph(
						hospitalReceive.getTomograph().getQuantityTomograph() + quantitySend - quantityReceive
					);
				
				hospitalSend.getTomograph().setQuantityTomograph(
						hospitalSend.getTomograph().getQuantityTomograph() + quantityReceive - quantitySend
					);
				
				if (traderBalanceTomograph(hospitalReceive, hospitalSend) == true) {
					hospitalRepository.save(hospitalReceive);
					hospitalRepository.save(hospitalSend);
					result = true;
					
				}
				
				else if (isCapacity90(hospitalSend) || isCapacity90(hospitalReceive)) {
					hospitalRepository.save(hospitalReceive);
					hospitalRepository.save(hospitalSend);
					result = true;
				}
				
				
				return result;
			}
			
			
			
			public boolean resourseChangeTomograph(Long hospitalIdReceive, Long hospitalIdSend, double quantityReceive, double quantitySend) {
				boolean[] result = {false};
				Optional<Hospital> optionalHospitalI = hospitalRepository.findById(hospitalIdReceive);
				optionalHospitalI.ifPresent(hospitalReceive -> {
					Optional<Hospital> optionalHospitalII = hospitalRepository.findById(hospitalIdSend);
					optionalHospitalII.ifPresent(hospitalSend -> {				
						
						result[0] = this.realizeOperationExchangeTomograph(hospitalReceive, hospitalSend, quantityReceive, quantitySend);
						 
						
						
						
					});
					
				});
				
				
				if (result[0] == true) {
					logExchangeService.saveLogExchangeSerive(hospitalIdReceive,
							hospitalIdSend, "Tomógrafo", quantityReceive, quantitySend);			
					
				}
				
				return result[0];
			}
			
			
			
			//Ambulance
			
			
			
			public boolean traderBalanceAmbulance(Hospital hospitalReceive, Hospital hospitalSend) {
				boolean result = false;
				
				if (hospitalReceive.getAmbulance().finalScore() == hospitalSend.getAmbulance().finalScore()) {
					result = true;
				}
				
				return result;
			}
			
			
			public boolean realizeOperationExchangeAmbulance(Hospital hospitalReceive, Hospital hospitalSend, double quantityReceive, double quantitySend) {
				boolean result = false;
				
				
				if (quantitySend > hospitalSend.getAmbulance().getQuantityAmbulance()) {
					quantitySend = hospitalSend.getAmbulance().getQuantityAmbulance();
				}
				
				if (quantityReceive > hospitalReceive.getAmbulance().getQuantityAmbulance()) {
					quantityReceive = hospitalSend.getAmbulance().getQuantityAmbulance();
				}
				
				
				
				hospitalReceive.getAmbulance().setQuantityAmbulance(
						hospitalReceive.getAmbulance().getQuantityAmbulance() + quantitySend - quantityReceive
					);
				
				hospitalSend.getAmbulance().setQuantityAmbulance(
						hospitalSend.getAmbulance().getQuantityAmbulance() + quantityReceive - quantitySend
					);
				
				if (traderBalanceAmbulance(hospitalReceive, hospitalSend) == true) {
					hospitalRepository.save(hospitalReceive);
					hospitalRepository.save(hospitalSend);
					result = true;
					
				}
				
				else if (isCapacity90(hospitalSend) || isCapacity90(hospitalReceive)) {
					hospitalRepository.save(hospitalReceive);
					hospitalRepository.save(hospitalSend);
					result = true;
				}
				
				
				return result;
			}
			
			
			
			public boolean resourseChangeAmbulance(Long hospitalIdReceive, Long hospitalIdSend, double quantityReceive, double quantitySend) {
				boolean[] result = {false};
				Optional<Hospital> optionalHospitalI = hospitalRepository.findById(hospitalIdReceive);
				optionalHospitalI.ifPresent(hospitalReceive -> {
					Optional<Hospital> optionalHospitalII = hospitalRepository.findById(hospitalIdSend);
					optionalHospitalII.ifPresent(hospitalSend -> {				
						
						result[0] = this.realizeOperationExchangeAmbulance(hospitalReceive, hospitalSend, quantityReceive, quantitySend);
						 
						
						
						
					});
					
				});
				
				if (result[0] == true) {
					logExchangeService.saveLogExchangeSerive(hospitalIdReceive,
							hospitalIdSend, "Ambulância", quantityReceive, quantitySend);			
					
				}
				
				return result[0];
			}
			
	

}
