package com.lucas.testePhoebus.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lucas.testePhoebus.models.LogExchange;
import com.lucas.testePhoebus.repositories.LogExchangeRepository;

@Service
public class LogExchangeService {
	
	private LogExchangeRepository logExchangeRepository;
	
	@Autowired
	public LogExchangeService(LogExchangeRepository logExchangeRepository) {
		this.logExchangeRepository = logExchangeRepository;
	}
	
	public void saveLogExchangeSerive(long idHospitalReceive,
			long idHospitalSend, String nameResource,
			double quantityReceive, double quantitySend) {
		
		LogExchange logExchange = new LogExchange();
		logExchange.setIdHospitalReceive(idHospitalReceive);
		logExchange.setIdHospitalSend(idHospitalSend);
		logExchange.setNameResource(nameResource);
		logExchange.setQuantityReceive(quantityReceive);
		logExchange.setQuantitySend(quantitySend);
		
		logExchangeRepository.save(logExchange);
	}

}
