package com.lucas.testePhoebus.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospitalTransferRespiratorDTO {
	
	Long hospitalSend;
	Long hospitalReceive;
	double quantitySend;
	double quantityReceive;

}
