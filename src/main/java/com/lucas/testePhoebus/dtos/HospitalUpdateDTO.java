package com.lucas.testePhoebus.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class HospitalUpdateDTO {
	Long id;
	String name;
	String address;
	String cnpj;
	String latitude;
	String longitude;
	double percentageCapacity;
	

}
