package com.lucas.testePhoebus.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HospitalTransferPhysicianDTO {
	
	Long hospitalSend;
	Long hospitalReceive;
	double quantitySend;
	double quantityReceive;

}
