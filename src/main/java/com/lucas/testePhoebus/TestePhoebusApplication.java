package com.lucas.testePhoebus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestePhoebusApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestePhoebusApplication.class, args);
	}

}
