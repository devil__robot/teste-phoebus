package com.lucas.testePhoebus.models;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class Respirator implements Serializable {
	private static final String name = "Respirador";	
	private static final double score = 5;	
	private double quantityRespirator = 0;
	
	public double finalScore() {
		if (quantityRespirator < 0) {
			quantityRespirator = 0;
		}
		return (quantityRespirator * score);
	}	

}
