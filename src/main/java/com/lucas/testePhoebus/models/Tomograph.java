package com.lucas.testePhoebus.models;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class Tomograph implements Serializable {
	
	private static final String name = "Tomógrafo";	
	private static final double score = 12;	
	private double quantityTomograph = 0;
	
	public double finalScore() {
		if (quantityTomograph < 0) {
			quantityTomograph = 0;
		}
		return (quantityTomograph * score);
	}	


}
