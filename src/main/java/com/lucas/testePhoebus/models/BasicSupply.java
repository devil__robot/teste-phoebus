package com.lucas.testePhoebus.models;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

import lombok.Data;

@Data
@Embeddable
public class BasicSupply {
	
	private String name;	
	private String shortDescription;	
	private BigDecimal score;	
	private BigDecimal quantity;	

}
