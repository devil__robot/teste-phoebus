package com.lucas.testePhoebus.models;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class Nurse implements Serializable {
	
	private static final String name = "Enfermeiro";	
	private static final double score = 3;	
	private double quantityNurse = 0;
	
	public double finalScore() {
		if (quantityNurse < 0) {
			quantityNurse = 0;
		}
		return (quantityNurse * score);
	}	
	

}
