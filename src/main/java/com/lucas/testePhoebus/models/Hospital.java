package com.lucas.testePhoebus.models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="hospitals")
public class Hospital implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String name;
	private String address;
	private String cnpj;
	private String latitude;
	private String longitude;
	private double percentageCapacity;
	private Date superLotationDate;
	private Date underLotationDate;
	
	@Embedded
	private Physician physician;
	
	@Embedded
	private Nurse nurse;
	
	@Embedded
	private Respirator respirator;
	
	@Embedded
	private Tomograph tomograph;
	
	@Embedded
	private Ambulance ambulance;
	
	
	@PreUpdate
	@PrePersist
	public void setDLotationDates() {
		if (this.percentageCapacity >= 90) {
			this.superLotationDate = new Date();
		}
		else {
			this.superLotationDate = null;
		}
		if (this.percentageCapacity<90) {
			this.underLotationDate = new Date();
		}
		else {
			this.underLotationDate = null;
		}
	}
	
	
}
