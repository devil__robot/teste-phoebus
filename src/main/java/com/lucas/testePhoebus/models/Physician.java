package com.lucas.testePhoebus.models;
import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class Physician implements Serializable {
	
	private static final String name = "Médico";	
	private static final double score = 3;	
	private double quantityPhysician = 0;
	
	public double finalScore() {
		if (quantityPhysician < 0) {
			quantityPhysician = 0;
		}
		return (quantityPhysician * score);
	}	
	
	

}
