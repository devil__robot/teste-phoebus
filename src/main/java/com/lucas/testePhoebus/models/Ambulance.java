package com.lucas.testePhoebus.models;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class Ambulance implements Serializable{
	
	private static final String name = "Ambulância";	
	private static final double score = 10;	
	private double quantityAmbulance = 0;
	
	public double finalScore() {
		if (quantityAmbulance < 0) {
			quantityAmbulance = 0;
		}
		return (quantityAmbulance * score);
	}	

}
