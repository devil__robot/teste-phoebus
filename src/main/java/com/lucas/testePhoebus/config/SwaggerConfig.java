package com.lucas.testePhoebus.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.util.regex.*;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
	public Docket api() {
	    return new Docket(DocumentationType.SWAGGER_2)
	        .select()
	        .apis(RequestHandlerSelectors.basePackage("com.lucas.testePhoebus"))
	        .paths(PathSelectors.any())
	        .build()
	        .apiInfo(apiInfo());

	}
	private ApiInfo apiInfo() {
	    return new ApiInfoBuilder()
	            .title("Spring Boot REST API - Pandemic Combat Aid System")
	            .description(
	            		"Aplicação Spring Boot REST API.\n"
	            		+"Desenvolvida para o teste de competência para"
        				+" o cargo de Desenvolvedor Back-End do "
	            		+"Ente Empresarial: Phoebus.\n"
	            		)
	            .version("1.0.0")
	            .license("Apache License Version 2.0")
	            .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
	            .contact(new Contact("Lucas Lins",
	            		"https://gitlab.com/devil__robot/teste-phoebus",
	            		"lucas.fsl.92@gmail.com"))
	            .build();
	}
}