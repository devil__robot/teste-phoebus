package com.lucas.testePhoebus.resources;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lucas.testePhoebus.models.Hospital;
import com.lucas.testePhoebus.models.LogExchange;
import com.lucas.testePhoebus.repositories.LogExchangeRepository;
import com.lucas.testePhoebus.services.LogExchangeService;
import com.lucas.testePhoebus.services.ReportService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/api")
@Api(value="API Rest Relatórios")
@CrossOrigin(origins="*")
public class ReportResource {
	
	@Autowired
	ReportService reportService;
	
	@Autowired
	LogExchangeService logExchangeService;
	
	@Autowired
	LogExchangeRepository logExchangeRepository;
	
	
	
	@GetMapping("/report/hospital_full")
	@ApiOperation(value="Retorna uma Lista dos Hospitais "
			+"que estão com sua capacidade igual ou acima "
			+"de 90 %.\n"
			+"NÃO é necessária passar parâmetros.\n"
			+"O retorno será 1 List<Hospital>\n"
			)
	public List<Hospital> reportHospitalsFull(){
		return reportService.listHospitalFull();
		
	}
	
	
	@GetMapping("/report/hospital_empity")
	@ApiOperation(value="Retorna uma Lista dos Hospitais "
			+"que estão com sua capacidade inferior "
			+"a 90 %.\n"
			+"NÃO é necessária passar parâmetros.\n"
			+"O retorno será 1 List<Hospital>\n"
			)
	public List<Hospital> reportHospitalsEmpity(){
		return reportService.listHospitalEmpity();
		
	}
	
	
	
	@GetMapping("/report/hospital_full_percentage")
	@ApiOperation(value="Retorna a percentage "
			+"dos Hospitais que estão com sua capacidade igual ou acima "
			+"de 90 %.\n"
			+"NÃO é necessária passar parâmetros.\n"
			+"O retorno será 1 Double.\n"
			)
	public double reportHospitalsFullPercentage(){
		return reportService.percentageHospitalFull();
		
	}
	
	
	
	@GetMapping("/report/hospital_empity_percentage")
	@ApiOperation(value="Retorna a percentage "
			+"dos Hospitais que estão com sua capacidade inferior "
			+"a 90 %.\n"
			+"NÃO é necessária passar parâmetros.\n"
			+"O retorno será 1 Double.\n"
			)
	public double reportHospitalsEmpityPercentage(){
		return reportService.percentageHospitalEmpity();
		
	}
	
	
	
	@GetMapping("/report/hospital_average_resource")
	@ApiOperation(value="Retorna a média "
			+"de cada um dos Recursos, "
			+"considerando TODOS os Hospitais Cadastrados.\n"
			+"NÃO é necessária passar parâmetros.\n"
			+"O retorno será 1 List<Double>.\n"
			+ "Os valores exibidos são respectivamente:\n"
			+ "Ambulância; Enfermeiro; Médico; Respirador; Tomográfo.\n"
			)
	public List<Double> averageRecoursePerHospital(){
		return reportService.averageRecoursePerHospital();
		
	}
	
	@GetMapping("/report/old_hospital_full")
	@ApiOperation(value="Retorna o Hospital Cadastrado "
			+"que está há MAIS TEMPO com sua capacidade, "
			+"igual ou superior a 90%.\n"
			+"NÃO é necessária passar parâmetros.\n"
			+"O retorno será 1 Hospital.\n"
			)
	public Hospital oldHospitalFull() {
		return reportService.oldHospitalFull();
	}
	
	
	
	@GetMapping("/report/old_hospital_empity")
	@ApiOperation(value="Retorna o Hospital Cadastrado "
			+"que está há MAIS TEMPO com sua capacidade, "
			+"inferior a 90%.\n"
			+"NÃO é necessária passar parâmetros.\n"
			+"O retorno será 1 Hospital.\n"
			)
	public Hospital oldHospitalEmpty() {
		return reportService.oldHospitalUnder();
	}
	
	
	@GetMapping("/report/logs")
	@ApiOperation(value="Retorna os Logs Armazenados.\n"
			+"Esses Logs são referentes as trocas de recursos "
			+"entre os Hospitais.\n"
			+"NÃO é necessária passar parâmetros.\n"
			+"O retorno será 1 List<Logs>.\n"
			+"Cada Log possuirá:\n"
			+"id, para que possa pesquisar por IDs futuramente.\n"
			+"idHospitalReceive, para saber qual o Hospital que recebeu.\n"
			+"idHospitalSend, para saber qual o Hospital que enviou.\n"
			+"nameResource, para saber qual o Recurso.\n"
			+"quantityReceive e quantitySend, para saber quanto "
			+"cada Hospital enviou e/ou recebeu.\n"
			+"dateOfOperation, para saber QUANDO a operação ocorreu.\n"
			)
	public List<LogExchange> logsExchanges() {
		return logExchangeRepository.findAll();
	}
	
	

}
