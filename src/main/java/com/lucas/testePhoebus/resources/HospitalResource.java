package com.lucas.testePhoebus.resources;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lucas.testePhoebus.dtos.HospitalTransferAmbulanceDTO;
import com.lucas.testePhoebus.dtos.HospitalTransferNurseDTO;
import com.lucas.testePhoebus.dtos.HospitalTransferPhysicianDTO;
import com.lucas.testePhoebus.dtos.HospitalTransferRespiratorDTO;
import com.lucas.testePhoebus.dtos.HospitalTransferTomographDTO;
import com.lucas.testePhoebus.dtos.HospitalUpdateDTO;
import com.lucas.testePhoebus.models.Hospital;
import com.lucas.testePhoebus.repositories.HospitalRepository;
import com.lucas.testePhoebus.services.HospitalService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/api")
@Api(value="API Rest Hospitais")
@CrossOrigin(origins="*")
public class HospitalResource {
	
	@Autowired
	HospitalRepository hospitalRepository;
	
	@Autowired
	HospitalService hospitalService;
	
	@GetMapping("/hospitals")
	@ApiOperation(value="Lista todos os Hospitais Cadastrados."
			+"\n"
			+"Não é preciso passar nenhum parâmetro."
			+"\n"
			+"O retorno será 1 Lista do tipo Hospital"
			)
	public List<Hospital> listHospital(){
		return hospitalRepository.findAll();
		
	}
	
	@GetMapping("/hospital/{id}")
	@ApiOperation(value="Retorna 1 Hospital Cadastrado."
			+"\n"
			+"É necessário passar ID válido."
			+"\n"
			+"O retorno será 1 Objeto do tipo Hospital"
			)
	public Hospital listHospitalById(@PathVariable(value="id") long id) {
		return hospitalRepository.findById(id);
	}
	
	
	
	@PostMapping("/hospital")
	@ApiOperation(value="Cadastra 1 Hospital.\n"			
			+"É necessário uma lista de parâmetros.\n"			
			+"O retorno será o Objeto cadastrado.\n"
			)
	public Hospital saveHospital(@RequestBody Hospital hospital) {
		return hospitalRepository.save(hospital);
	}
	
	
	
	
	@PutMapping("/hospital")
	@ApiOperation(value="Atualiza 1 Hospital.\n"			
			+"É necessária passar uma lista de parâmetros.\n"
			+"INCLUSIVE um ID válido!"
			+"O retorno será uma String.\n"
			+"Com uma mensagem de Sucesso ou de Fracsso ao Atualizar.\n"
			+"Fora criado um tipo específico de DTO para garantir que"
			+" alguns valores permanecessem inálterados!\n"
			)
	public String  updateHospital(@RequestBody HospitalUpdateDTO hospitalDTO) {
		String[] result = {"Não foi possível atualizar"};
		Optional<Hospital> optionalHospital = hospitalRepository.findById(hospitalDTO.getId());
		optionalHospital.ifPresent(hospital -> {
			hospital.setName(hospitalDTO.getName());
			hospital.setAddress(hospitalDTO.getAddress());
			hospital.setCnpj(hospitalDTO.getCnpj());
			hospital.setLatitude(hospitalDTO.getLatitude());
			hospital.setLongitude(hospitalDTO.getLongitude());
			hospital.setPercentageCapacity(hospitalDTO.getPercentageCapacity());
			hospitalRepository.save(hospital);
			result[0] = "Atualização Realizada!";
		});
		return result[0];
	}
	
	@PostMapping("/exchange/physician")
	@ApiOperation(value="Realiza a troca dos Médicos entre 2 Hospitais.\n"			
			+"É necessária passar o ID de Cada Hospital.\n"
			+"IDs válidos!"
			+"É necessário passar mais 2 válores.\n"
			+"Cada valor será a quantidade de recurso trocado por cada Hospital.\n"
			+"O retorno será um Boolean, "
			+"para facilitar a criação da resposta ideal pelo Front-End.\n"
			+"True caso a troca seja realizada.\n"
			+"False caso a troca NÃO seja realizada.\n"
			)
	public boolean exchangeHospitalPhysician(@RequestBody HospitalTransferPhysicianDTO hospitalTransferDTO) {
		
		boolean result =  hospitalService.resourseChangePhysician
		(
				hospitalTransferDTO.getHospitalReceive(),
				hospitalTransferDTO.getHospitalSend(),
				hospitalTransferDTO.getQuantityReceive(),
				hospitalTransferDTO.getQuantitySend()
		);
		
		return result;
	}
	
	
	@PostMapping("/exchange/nurse")
	@ApiOperation(value="Realiza a troca dos Enfermeiros entre 2 Hospitais.\n"			
			+"É necessária passar o ID de Cada Hospital.\n"
			+"IDs válidos!"
			+"É necessário passar mais 2 válores.\n"
			+"Cada valor será a quantidade de recurso trocado por cada Hospital.\n"
			+"O retorno será um Boolean, "
			+"para facilitar a criação da resposta ideal pelo Front-End.\n"
			+"True caso a troca seja realizada.\n"
			+"False caso a troca NÃO seja realizada.\n"
			)
	public boolean exchangeHospitalNurse(@RequestBody HospitalTransferNurseDTO hospitalTransferDTO) {
		
		boolean result =  hospitalService.resourseChangeNurse
		(
				hospitalTransferDTO.getHospitalReceive(),
				hospitalTransferDTO.getHospitalSend(),
				hospitalTransferDTO.getQuantityReceive(),
				hospitalTransferDTO.getQuantitySend()
		);
		
		return result;
	}
	
	
	@PostMapping("/exchange/respirator")
	@ApiOperation(value="Realiza a troca dos Respiradores entre 2 Hospitais.\n"			
			+"É necessária passar o ID de Cada Hospital.\n"
			+"IDs válidos!"
			+"É necessário passar mais 2 válores.\n"
			+"Cada valor será a quantidade de recurso trocado por cada Hospital.\n"
			+"O retorno será um Boolean, "
			+"para facilitar a criação da resposta ideal pelo Front-End.\n"
			+"True caso a troca seja realizada.\n"
			+"False caso a troca NÃO seja realizada.\n"
			)
	public boolean exchangeHospitalRespirator(@RequestBody HospitalTransferRespiratorDTO hospitalTransferDTO) {
		
		boolean result =  hospitalService.resourseChangeRespirator
		(
				hospitalTransferDTO.getHospitalReceive(),
				hospitalTransferDTO.getHospitalSend(),
				hospitalTransferDTO.getQuantityReceive(),
				hospitalTransferDTO.getQuantitySend()
		);
		
		return result;
	}
	
	
	@PostMapping("/exchange/tomograph")
	@ApiOperation(value="Realiza a troca dos Tomógrafos entre 2 Hospitais.\n"			
			+"É necessária passar o ID de Cada Hospital.\n"
			+"IDs válidos!"
			+"É necessário passar mais 2 válores.\n"
			+"Cada valor será a quantidade de recurso trocado por cada Hospital.\n"
			+"O retorno será um Boolean, "
			+"para facilitar a criação da resposta ideal pelo Front-End.\n"
			+"True caso a troca seja realizada.\n"
			+"False caso a troca NÃO seja realizada.\n"
			)
	public boolean exchangeHospitalTomograph(@RequestBody HospitalTransferTomographDTO hospitalTransferDTO) {
		
		boolean result =  hospitalService.resourseChangeTomograph
		(
				hospitalTransferDTO.getHospitalReceive(),
				hospitalTransferDTO.getHospitalSend(),
				hospitalTransferDTO.getQuantityReceive(),
				hospitalTransferDTO.getQuantitySend()
		);
		
		return result;
	}
	
	
	@PostMapping("/exchange/ambulance")
	@ApiOperation(value="Realiza a troca das Ambulâncias entre 2 Hospitais.\n"			
			+"É necessária passar o ID de Cada Hospital.\n"
			+"IDs válidos!"
			+"É necessário passar mais 2 válores.\n"
			+"Cada valor será a quantidade de recurso trocado por cada Hospital.\n"
			+"O retorno será um Boolean, "
			+"para facilitar a criação da resposta ideal pelo Front-End.\n"
			+"True caso a troca seja realizada.\n"
			+"False caso a troca NÃO seja realizada.\n"
			)
	public boolean exchangeHospitalAmbulance(@RequestBody HospitalTransferAmbulanceDTO hospitalTransferDTO) {
		
		boolean result =  hospitalService.resourseChangeAmbulance
		(
				hospitalTransferDTO.getHospitalReceive(),
				hospitalTransferDTO.getHospitalSend(),
				hospitalTransferDTO.getQuantityReceive(),
				hospitalTransferDTO.getQuantitySend()
		);
		
		return result;
	}
	

}
